import React, { Component } from 'react';
import { ScrollView } from 'react-native';
import axios from 'axios';
import AlbumDetail from './AlbumDetail';

class AlbumList extends Component {
    state = { albums: [] };
    componentWillMount() {
        //with axios
        axios.get('https://rallycoding.herokuapp.com/api/music_albums').then((data) => {
            this.setState({ albums: data.data });
        }).catch((err) => {
            console.error(err);
        });
        //native fetch api
        // fetch('https://rallycoding.herokuapp.com/api/music_albums').then(res => res.json()).then((data) => {
        //     console.log(data);
        // });
    }
    renderAlbums() {
        return this.state.albums.map(album =>
            <AlbumDetail key={album.title} album={album} />);
    }
    render() {
        console.log(this.state);
        return (
            <ScrollView>
                {this.renderAlbums()}
            </ScrollView>
        );
    }   
}

export default AlbumList;
