import React, { Component } from 'react';
import firebase from 'firebase';
import { Text } from 'react-native';
import { Button, Card, CardSection, Input, Spinner } from './common';

class LoginForm extends Component {
    state = { email: '', password: '', err: '', loading: false };

    onButtonPress() {
        const { email, password } = this.state;
        this.setState({ err: '', loading: true });
        firebase.auth().signInWithEmailAndPassword(email.trim(), password)
        .then(this.onLoadingSuccess.bind())
        .catch((error) => {
            console.log(error);
            // if (error) {
            //     if (error.code === 'auth/wrong-password') {
            //         this.setState({ err: error.message });
            //     } else {
            //         firebase.auth().createUserWithEmailAndPassword(email.trim(), password)
            //         .then(this.onLoadingSuccess.bind(this))
            //         .catch(this.onLoadingFail.bind(this));
            //     }
            // }
            firebase.auth().createUserWithEmailAndPassword(email.trim(), password)
                    .then(this.onLoadingSuccess.bind(this))
                    .catch(this.onLoadingFail.bind(this));
        });
    }

    onLoadingSuccess() {
        this.setState({ loading: 'false', email: '', password: '', error: '' });
    }

    onLoadingFail() {
        this.setState({ error: 'Authentication failed.', loading: false });
    }

    renderButton() {
        let com = '';
        if (this.state.loading) {
            com = <Spinner size='small' />;
        } else {
            com = <Button onPressed={this.onButtonPress.bind(this)}>Log in</Button>;
        }
        return com;
    }

    
    render() {
        return (
            <Card>
                <CardSection >
                    <Input
                        label='Email'
                        value={this.state.email}
                        onChangeText={email => this.setState({ email })}
                        placeholder='user@user.com'
                    />

                
                </CardSection>
                <CardSection >
                    <Input 
                        secureTextEntry
                        label='Password'
                        placeholder='password'
                        value={this.state.password}
                        onChangeText={password => this.setState({ password })}
                    />
                </CardSection>
                <Text style={styles.errorText}>
                    {this.state.err}
                </Text>
                <CardSection >
                    {this.renderButton()}
                </CardSection>
            </Card>
        );
    }
}

const styles = {
    errorText: {
        fontSize: 13,
        alignSelf: 'center',
        color: 'red'
    }
};

export { LoginForm };
