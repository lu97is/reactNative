/**
 * Sample React Native App
 * https://github.com/facebook/react-native
 * @flow
 */

import React, { Component } from 'react';
import {
  View,
} from 'react-native';
import firebase from 'firebase';
import { Header, Button, CardSection, Spinner } from './components/common';
import { LoginForm } from './components/LoginForm';

class App extends Component {
  state = { loggedIn: null }
  componentWillMount() {
    try {
      firebase.initializeApp(
        {
          apiKey: 'AIzaSyAwdoOPk95pS4-ABatXqvAP10nVcVfqZ0s',
          authDomain: 'authreact-f1eb7.firebaseapp.com',
          databaseURL: 'https://authreact-f1eb7.firebaseio.com',
          projectId: 'authreact-f1eb7',
          storageBucket: 'authreact-f1eb7.appspot.com',
          messagingSenderId: '271308947457'
        }
      );
    } catch (error) {
      console.log(error);
    }
    firebase.auth().onAuthStateChanged((user) => {
      if (user) {
        this.setState({ loggedIn: true });
      } else {
        this.setState({ loggedIn: false });
      }
    });
  }
  renderContent() {
    switch (this.state.loggedIn) {
      case true:
        return (
          <CardSection>
            <Button onPressed={() => firebase.auth().signOut()}>Log out</Button>
          </CardSection>
        );
      case false:
        return <LoginForm />;
      default:
        return <CardSection><Spinner size='large' /></CardSection>;
    }
}

  render() {
    return (
      <View>
        <Header headerText='Auth' />
        {this.renderContent()}
      </View>
    );
  }
}

export default App;

