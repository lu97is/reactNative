import React, { Component } from 'react';
import { Provider } from 'react-redux';
import { createStore, applyMiddleware } from 'redux';
import firebase from 'firebase';
import ReduxThunk from 'redux-thunk';
import reducers from './src/reducers';
import Router from './src/Router';

class App extends Component {
  componentWillMount() {
    try {
      firebase.initializeApp(
        {
          apiKey: 'AIzaSyAwdoOPk95pS4-ABatXqvAP10nVcVfqZ0s',
          authDomain: 'authreact-f1eb7.firebaseapp.com',
          databaseURL: 'https://authreact-f1eb7.firebaseio.com',
          projectId: 'authreact-f1eb7',
          storageBucket: 'authreact-f1eb7.appspot.com',
          messagingSenderId: '271308947457'
        }
      );
    } catch (error) {
      console.log(error);
    }
  }
  render() {
    return (
      <Provider store={createStore(reducers, {}, applyMiddleware(ReduxThunk))}>
        <Router />
      </Provider>
    );
  }
}

export default App;
