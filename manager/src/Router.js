import React from 'react';
import { Scene, Router } from 'react-native-router-flux';
import LoginForm from './components/LoginForm';
import EmployeeList from './components/EmployeeList';

const RouterComponent = () => {
    return (
        <Router>
            <Scene key='root' hideNavBar>
                <Scene key='auth'>
                    <Scene key='login' initial component={LoginForm} title='Please Login' />
                </Scene>
                <Scene key='main'>
                    <Scene
                     key='employeeList' 
                     component={EmployeeList} 
                     title="Employees" 
                     rightTitle='Add'
                     rightButtonTextStyle={{ right: 0 }}
                     onRight={() => console.log('right')}
                    />
                </Scene>
            </Scene>
        </Router>
    );
};

export default RouterComponent;
