import React, { Component } from 'react';
import { FlatList } from 'react-native';
import { connect } from 'react-redux';
import ListItem from './ListItem';

class LibraryList extends Component {
    componentWillMount() {
        // const ds = new FlatList.DataSource({
        //     rowHasChanged: (r1, r2) => r1 !== r2
        // });

        // this.dataSource = ds.cloneWithRows(this.props.libraries);
    }

    renderRow(library) {
        return <ListItem library={library} />;
    }
    
    render() {
        return (
            <FlatList 
                data={this.props.libraries}
                renderItem={({ item }) => this.renderRow(item)}
                keyExtractor={(library) => library.id.toString()}
            />
        );
    }
}

const mapStateToProps = state => ({ libraries: state.libraries });

export default connect(mapStateToProps)(LibraryList);
